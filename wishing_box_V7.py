def recLED(onoff: str):
    if onoff == "on":
        pins.digital_write_pin(DigitalPin.P0, 1)
    else:
        pins.digital_write_pin(DigitalPin.P0, 0)

def on_on_event():
    sensityLow()
control.on_event(5000, 5002, on_on_event)

def sensityLow():
    global thresAttack, thresRelease, RFupdatePauseCnt
    thresAttack = 120
    thresRelease = 70
    RFupdatePauseCnt = 2
    radio.send_string("CLEAR")
    radio.send_string("sensitity low")
    radio.send_string("120 70")

def on_on_event2():
    basic.clear_screen()
    while isRecording:
        recLED("on")
        led.plot(2, 2)
        basic.pause(50)
        recLED("off")
        led.unplot(2, 2)
        basic.pause(50)
control.on_event(4800, 4801, on_on_event2)

def sensityHigh():
    global thresAttack, thresRelease, RFupdatePauseCnt
    thresAttack = 80
    thresRelease = 30
    RFupdatePauseCnt = 2
    radio.send_string("CLEAR")
    radio.send_string("sensitity high")
    radio.send_string("80 30")
def startRec():
    global isRecording, RFupdatePauseCnt
    pins.digital_write_pin(DigitalPin.P1, 1)
    isRecording = True
    RFupdatePauseCnt = 9
    radio.send_string("CLEAR")
    radio.send_string("recording...")
def stopRec():
    global isRecording, stopRecDebounce, RFupdatePauseCnt
    pins.digital_write_pin(DigitalPin.P1, 0)
    isRecording = False
    stopRecDebounce = 0
    RFupdatePauseCnt = 2
    radio.send_string("REC stopped")
    basic.show_icon(IconNames.DUCK)

def on_on_event3():
    sensityHigh()
control.on_event(5000, 5001, on_on_event3)

def on_on_event4():
    sensityMed()
control.on_event(5000, 5003, on_on_event4)

def on_on_event5():
    basic.pause(1000)
    while isRecording:
        basic.pause(10)
    triggerPlay()
control.on_event(4800, 4803, on_on_event5)

# 160*50 = 8000 ms = 8 sec
# rec tmo

def on_on_event6():
    for index in range(160):
        basic.pause(50)
        if not (isRecording):
            break
    stopRec()
control.on_event(4800, 4802, on_on_event6)

def sensityMed():
    global thresAttack, thresRelease, RFupdatePauseCnt
    thresAttack = 100
    thresRelease = 50
    RFupdatePauseCnt = 2
    radio.send_string("CLEAR")
    radio.send_string("sensitity med")
    radio.send_string("100 50")

def on_on_event7():
    if not (isRecording) and isRecReady:
        triggerPlay()
control.on_event(5000, 5004, on_on_event7)

def initIO():
    pins.digital_write_pin(DigitalPin.P1, 0)
    pins.digital_write_pin(DigitalPin.P2, 0)
    recLED("on")
    basic.pause(500)
    recLED("off")
def triggerPlay():
    global isRecReady, RFupdatePauseCnt
    isRecReady = False
    recLED("on")
    RFupdatePauseCnt = 9
    radio.send_string("CLEAR")
    radio.send_string("playing...")
    pins.digital_write_pin(DigitalPin.P2, 1)
    basic.pause(500)
    pins.digital_write_pin(DigitalPin.P2, 0)
    basic.pause(5000)
    recLED("off")
    radio.send_string("CLEAR")
    radio.send_string("")
    radio.send_string("")
    radio.send_string("ready....")
    isRecReady = True
    RFupdatePauseCnt = 2
sndLev = 0
thresRelease = 0
thresAttack = 0
RFupdatePauseCnt = 0
stopRecDebounce = 0
isRecording = False
isRecReady = False
initIO()
recLED("on")
radio.set_group(180)
led.set_brightness(5)
isRecReady = False
isRecording = False
stopRecDebounce = 0
RFupdatePauseCnt = 0
stopRec()
radio.send_string("CLEAR")
radio.send_string("")
radio.send_string("wishing box V7")
radio.send_string("Spring Festival")
radio.send_string("2023 1 22")
radio.send_string("22:10")
basic.pause(2000)
sensityLow()
basic.pause(2000)
isRecReady = True
recLED("off")

def on_every_interval():
    global RFupdatePauseCnt
    if RFupdatePauseCnt > 0:
        RFupdatePauseCnt += -1
    else:
        radio.send_value("sndLev", sndLev)
loops.every_interval(1000, on_every_interval)

def on_every_interval2():
    if isRecReady:
        recLED("on")
        basic.pause(50)
        recLED("off")
loops.every_interval(2000, on_every_interval2)

def on_every_interval3():
    global sndLev, stopRecDebounce
    sndLev = input.sound_level()
    if isRecording:
        if sndLev <= thresRelease:
            stopRecDebounce += 1
            if stopRecDebounce >= 10:
                stopRec()
        else:
            stopRecDebounce = 0
    elif sndLev >= thresAttack:
        if isRecReady:
            startRec()
            # blink REC LED
            control.raise_event(4800, 4801)
            # 8 sec rec tmo
            control.raise_event(4800, 4802)
            # playing after rec done
            control.raise_event(4800, 4803)
loops.every_interval(100, on_every_interval3)
