def on_button_pressed_a():
    basic.show_string("A")
    radio.raise_event(5000, 5001)
input.on_button_pressed(Button.A, on_button_pressed_a)

def on_button_pressed_ab():
    basic.show_string("D")
    radio.raise_event(5000, 5003)
input.on_button_pressed(Button.AB, on_button_pressed_ab)

def on_received_string(receivedString):
    if receivedString == "CLEAR":
        OLED.clear()
    else:
        OLED.write_string_new_line(receivedString)
radio.on_received_string(on_received_string)

def on_button_pressed_b():
    basic.show_string("B")
    radio.raise_event(5000, 5002)
input.on_button_pressed(Button.B, on_button_pressed_b)

def on_received_value(name, value):
    global showCnt
    if name == "sndLev":
        showCnt += 1
        if showCnt >= 3:
            showCnt = 0
            OLED.clear()
            OLED.write_string_new_line("")
        OLED.write_num_new_line(value)
        OLED.write_string_new_line("")
        serial.write_number(value)
        serial.write_line("")
radio.on_received_value(on_received_value)

def on_logo_touched():
    basic.show_string("L")
    radio.raise_event(5000, 5004)
input.on_logo_event(TouchButtonEvent.TOUCHED, on_logo_touched)

showCnt = 0
radio.set_group(180)
led.set_brightness(16)
OLED.init(128, 64)
OLED.write_string_new_line("wishing box tester")
OLED.write_string_new_line("Spring Festival")
OLED.write_string_new_line("2023 1 22")
OLED.write_string_new_line("Freda Li")
basic.pause(5000)
showCnt = 0